{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE InstanceSigs #-}

module Config where

import           Control.Applicative ((<|>))
import           Control.Exception (Exception, try, throw)
import           Control.Monad (join, when)
import           Data.Aeson (FromJSON(parseJSON), toJSON, eitherDecode)
import           Data.Aeson.Types (parseEither)
import           Data.Bifunctor (bimap, first, second)
import           Data.ByteString.Lazy  as DBL
import           Data.Functor.Identity
import           Data.Maybe (fromMaybe, isJust)
import           Data.Monoid
import           Data.Text as DT
import           Data.Text.IO as DTI
import           GHC.Generics
import           Text.Parsec.Error (ParseError)
import           Text.Read (readMaybe)
import           Text.Toml (parseTomlDoc)
import           Util (rightOrThrow)
import qualified Filesystem.Path as FP
import qualified Filesystem.Path.CurrentOS  as FPCOS

defaultHost :: Host
defaultHost = "localhost"

defaultPort :: Port
defaultPort = 5000

defaultTaskStoreFilePath :: FilePath
defaultTaskStoreFilePath = ":memory:"

defaultEntityStoreFilePath :: FilePath
defaultEntityStoreFilePath = ":memory:"

type Host = String
type Port = Int
newtype ProcessEnvironment = ProcessEnvironment {getProcessEnv :: [(String, String)]} deriving (Eq)

data ConfigurationError = ConfigParseError String
                        | TOMLParserError ParseError
                        | InvalidConfigError String
                        | InvalidPath FP.FilePath String
                          deriving (Eq)

deriving instance Show ConfigurationError
deriving instance Exception ConfigurationError

-- | Parametric polymorphism over type f (e.g. `Identity` or `Maybe`)
data AppConfig f  = AppConfig
    { host            :: f Host
    , port            :: f Port
    , taskStoreConfig :: f (TaskStoreConfig f)
    , entityStoreConfig :: f (EntityStoreConfig f)
    }

type CompleteAppConfig = AppConfig Identity
deriving instance Generic CompleteAppConfig
deriving instance Eq CompleteAppConfig
deriving instance Show CompleteAppConfig
deriving instance FromJSON CompleteAppConfig

type PartialAppConfig = AppConfig Maybe
deriving instance Generic PartialAppConfig
deriving instance Eq PartialAppConfig
deriving instance Show PartialAppConfig
deriving instance FromJSON PartialAppConfig

instance Semigroup CompleteAppConfig where
    a <> b = b

instance Monoid CompleteAppConfig where
    mempty = AppConfig (Identity defaultHost) (Identity defaultPort) (Identity mempty) (Identity mempty)

instance Semigroup PartialAppConfig where
    a <> b = AppConfig { host=resolveMaybes host
                       , port=resolveMaybes port
                       , taskStoreConfig=resolveMaybes taskStoreConfig
                       , entityStoreConfig=resolveMaybes entityStoreConfig
                       }
        where
          resolveMaybes :: (PartialAppConfig -> Maybe a) -> Maybe a
          resolveMaybes getter = getter b <|> getter a

instance Monoid PartialAppConfig where
    mempty = AppConfig Nothing Nothing Nothing Nothing

newtype TaskStoreConfig f = TaskStoreConfig { tscDBFilePath :: f FilePath }
newtype EntityStoreConfig f = EntityStoreConfig { escDBFilePath :: f FilePath }

type CompleteTaskStoreConfig = TaskStoreConfig Identity
deriving instance Generic CompleteTaskStoreConfig
deriving instance Eq CompleteTaskStoreConfig
deriving instance Show CompleteTaskStoreConfig
deriving instance FromJSON CompleteTaskStoreConfig

type PartialTaskStoreConfig = TaskStoreConfig Maybe
deriving instance Generic PartialTaskStoreConfig
deriving instance Eq PartialTaskStoreConfig
deriving instance Show PartialTaskStoreConfig
deriving instance FromJSON PartialTaskStoreConfig

type CompleteEntityStoreConfig = EntityStoreConfig Identity
deriving instance Generic CompleteEntityStoreConfig
deriving instance Eq CompleteEntityStoreConfig
deriving instance Show CompleteEntityStoreConfig
deriving instance FromJSON CompleteEntityStoreConfig

type PartialEntityStoreConfig = EntityStoreConfig Maybe
deriving instance Generic PartialEntityStoreConfig
deriving instance Eq PartialEntityStoreConfig
deriving instance Show PartialEntityStoreConfig
deriving instance FromJSON PartialEntityStoreConfig

instance Semigroup CompleteTaskStoreConfig where
    a <> b = b

instance Monoid CompleteTaskStoreConfig where
    mempty = TaskStoreConfig (Identity defaultTaskStoreFilePath)

instance Semigroup PartialTaskStoreConfig where
    a <> b = TaskStoreConfig { tscDBFilePath=resolveMaybes tscDBFilePath }
        where
          resolveMaybes :: (PartialTaskStoreConfig -> Maybe a) -> Maybe a
          resolveMaybes getter = getter a <|> getter b

instance Monoid PartialTaskStoreConfig where
    mempty = TaskStoreConfig Nothing

instance Semigroup CompleteEntityStoreConfig where
    a <> b = b

instance Monoid CompleteEntityStoreConfig where
    mempty = EntityStoreConfig (Identity defaultEntityStoreFilePath)

instance Semigroup PartialEntityStoreConfig where
    a <> b = EntityStoreConfig { escDBFilePath=resolveMaybes escDBFilePath }
        where
          resolveMaybes :: (PartialEntityStoreConfig -> Maybe a) -> Maybe a
          resolveMaybes getter = getter a <|> getter b

instance Monoid PartialEntityStoreConfig where
    mempty = EntityStoreConfig Nothing

----------
-- JSON --
----------

class (FromJSON cfg) => FromJSONFile cfg where
    fromJSONFile :: FP.FilePath -> IO (Either ConfigurationError cfg)

instance FromJSONFile PartialAppConfig where
    fromJSONFile path = decodeAndTransformError <$> DBL.readFile convertedPath
        where
          convertedPath = FPCOS.encodeString path

          decodeAndTransformError :: ByteString -> Either ConfigurationError PartialAppConfig
          decodeAndTransformError = first ConfigParseError . eitherDecode

----------
-- TOML --
----------

class (FromJSONFile cfg) => FromTOMLFile cfg where
    fromTOMLFile :: FP.FilePath -> IO (Either ConfigurationError cfg)

instance FromTOMLFile PartialAppConfig where
    fromTOMLFile path = flattenEither . convertAndParse . parseTOML
                        <$> DTI.readFile convertedPath
        where
          convertedPath = FPCOS.encodeString path
          parseTOML = first TOMLParserError . parseTomlDoc ""
          convertAndParse = second (parseEither parseJSON . toJSON)
          flattenEither v = case v of
                              Right (Right cfg) -> Right cfg
                              Right (Left err) -> Left (ConfigParseError err)
                              Left err -> Left err

---------
-- ENV --
---------

class FromENV cfg where
    fromENV :: ProcessEnvironment -> cfg

instance FromENV PartialAppConfig where
    fromENV pEnv = AppConfig { host=prop "TODO_HOST"
                             , port=readMaybe =<< prop "TODO_PORT"
                             , taskStoreConfig=Just $ fromENV pEnv
                             , entityStoreConfig=Just $ fromENV pEnv
                             }
        where
          prop :: String -> Maybe String
          prop = flip lookup (getProcessEnv pEnv)

instance FromENV PartialTaskStoreConfig where
    fromENV pEnv = TaskStoreConfig { tscDBFilePath=prop "TASK_STORE_FILE_PATH" }
        where
          prop :: String -> Maybe String
          prop = flip lookup (getProcessEnv pEnv)

instance FromENV PartialEntityStoreConfig where
    fromENV pEnv = EntityStoreConfig { escDBFilePath=prop "ENTITY_STORE_FILE_PATH" }
        where
          prop :: String -> Maybe String
          prop = flip lookup (getProcessEnv pEnv)

-- | The class of configurations that can absorb partials of themselves to maintain a whole
class AbsorbPartial complete partial  where
    absorbPartial :: complete -> partial -> complete

instance AbsorbPartial CompleteAppConfig PartialAppConfig where
    absorbPartial :: CompleteAppConfig -> PartialAppConfig -> CompleteAppConfig
    absorbPartial c p = AppConfig { host = maybe (host c) Identity (host p)
                                  , port = maybe (port c) Identity (port p)
                                  , taskStoreConfig = Identity $ absorbPartial tsc maybeTSC
                                  , entityStoreConfig = Identity $ absorbPartial esc maybeESC
                                  }
        where
          tsc = runIdentity $ taskStoreConfig c
          maybeTSC = fromMaybe mempty $ taskStoreConfig p

          esc = runIdentity $ entityStoreConfig c
          maybeESC = fromMaybe mempty $ entityStoreConfig p

instance AbsorbPartial CompleteTaskStoreConfig PartialTaskStoreConfig where
    absorbPartial :: CompleteTaskStoreConfig -> PartialTaskStoreConfig -> CompleteTaskStoreConfig
    absorbPartial c p = TaskStoreConfig { tscDBFilePath = maybe (tscDBFilePath c) Identity (tscDBFilePath p) }

instance AbsorbPartial CompleteEntityStoreConfig PartialEntityStoreConfig where
    absorbPartial :: CompleteEntityStoreConfig -> PartialEntityStoreConfig -> CompleteEntityStoreConfig
    absorbPartial c p = EntityStoreConfig { escDBFilePath = maybe (escDBFilePath c) Identity (escDBFilePath p) }

buildConfigWithDefault :: CompleteAppConfig -> [PartialAppConfig] -> CompleteAppConfig
buildConfigWithDefault orig partials = orig `absorbPartial` combinedPartials
    where
      combinedPartials :: PartialAppConfig
      combinedPartials = Prelude.foldl (<>) (mempty :: PartialAppConfig) partials

-- | The default configuration *is* a fully specified complete app config @ mempty state
appConfigDefault :: CompleteAppConfig
appConfigDefault = mempty

-- | The default configuration *is* a fully specified complete app config @ mempty state
defaultCompleteAppConfig :: CompleteAppConfig
defaultCompleteAppConfig = mempty

-- | The default configuration *is* a fully specified complete app config @ mempty state
defaultCompleteTaskStoreConfig :: CompleteTaskStoreConfig
defaultCompleteTaskStoreConfig = mempty

-- | The default configuration *is* a fully specified complete app config @ mempty state
defaultCompleteEntityStoreConfig :: CompleteEntityStoreConfig
defaultCompleteEntityStoreConfig = mempty

-- | The default partially specified configuration is mempty
defaultPartialAppConfig :: PartialAppConfig
defaultPartialAppConfig = mempty

-- | Build an App configuration from a given file, using system environment as well as
makeAppConfig :: Maybe Prelude.FilePath -> ProcessEnvironment -> IO (Either ConfigurationError CompleteAppConfig)
makeAppConfig maybeStrPath env = try generateConfig
    where
      maybePath :: Maybe FPCOS.FilePath
      maybePath = FPCOS.fromText . DT.pack <$> maybeStrPath

      extension :: Maybe (Maybe Text)
      extension = FP.extension <$> maybePath

      isJSONExtension = (==Just "json")
      isTOMLExtension = (==Just "toml")

      isJSONFile = maybe False isJSONExtension extension
      isTOMLFile = maybe False isTOMLExtension extension

      pathExtensionIsInvalid :: Bool
      pathExtensionIsInvalid = not $ isJSONFile || isTOMLFile

      pathInvalidExtensionErr :: ConfigurationError
      pathInvalidExtensionErr = InvalidPath (fromMaybe "<no path>" maybePath) "Path is invalid (must be either a .json or .toml path)"

      envCfg :: PartialAppConfig
      envCfg = fromENV env :: PartialAppConfig

      buildFromEnv :: IO CompleteAppConfig
      buildFromEnv = pure $ absorbPartial defaultCompleteAppConfig envCfg

      getFileConfig :: FPCOS.FilePath -> IO (Either ConfigurationError PartialAppConfig)
      getFileConfig = if isJSONFile then fromJSONFile else fromTOMLFile

      generateConfig :: IO CompleteAppConfig
      generateConfig = maybe buildFromEnv buildFromPathAndEnv maybePath

      buildFromPathAndEnv :: FPCOS.FilePath -> IO CompleteAppConfig
      buildFromPathAndEnv path = when pathExtensionIsInvalid (throw pathInvalidExtensionErr)
                                 >> getFileConfig path
                                 >>= rightOrThrow
                                 >>= \fileCfg -> pure (buildConfigWithDefault (mempty :: CompleteAppConfig) [fileCfg, envCfg])
