{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE DeriveGeneric #-}

module Util
    ( rightOrThrow )
where

import           Control.Exception (Exception, throw)

-- | Ensure that an Either resolves to it's Right value
rightOrThrow :: (Exception a) => Either a b -> IO b
rightOrThrow e = case e of
                   (Left err) -> throw err
                   (Right v) -> return v
