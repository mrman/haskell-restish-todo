{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE ExplicitForAll #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeInType #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeOperators #-}

module Components.EntityStore.SQLite
    (SQLiteEntityStore)
where

import           Control.Monad (ap)
import           Data.Proxy (Proxy)
import           Data.Maybe (isJust)
import           Control.Applicative((<|>))
import           Components.EntityStore.Migrations.SQLite (migrations)
import           Config (CompleteEntityStoreConfig, EntityStoreConfig(..))
import           Control.Exception (SomeException(..), throw, catch, try)
import           Data.Either (Either)
import           Data.Functor.Identity (Identity(..), runIdentity)
import           Data.List (sort, find)
import           Data.UUID (UUID, toText, fromText)
import           Data.UUID.V4 (nextRandom)
import           Database.SQLite.Simple
import           Database.SQLite.Simple.FromField (fieldData, ResultError(ConversionFailed), FieldParser, Field, FromField(..), returnError)
import           Database.SQLite.Simple.FromRow (RowParser)
import           Database.SQLite.Simple.ToField (ToField(..))
import           Types
import           Util (rightOrThrow)
import           NeatInterpolation (text)
import qualified Data.Text as DT

data SQLiteEntityStore = SQLiteEntityStore
    { stsCfg  :: CompleteEntityStoreConfig
    , stsConn :: Maybe Connection
    }

-- | SQLiteEntityStore component
--   start & stop are no-ops since this component does little more than bundle together some functionality (for now)
instance Component SQLiteEntityStore where
    start :: SQLiteEntityStore -> IO ()
    start s = migrate s
              >>= rightOrThrow
              >> pure ()

    stop :: SQLiteEntityStore -> IO ()
    stop s = pure ()

-- | A validated object's ToRow is just the same as it's contained object's ToRow
--   this can probably be auto-derived but let's write it manually for now.
instance ToRow a => ToRow (Validated a) where
    toRow = toRow . getValidatedObj

-- | UUIDs need to be converted to text before they can turn into fields
instance ToField UUID where
    toField = SQLText . toText

instance ToField TaskState where
    toField = SQLText . DT.pack . show

instance ToField a => ToField (Identity a) where
    toField = toField . runIdentity

-- | ToRow (WithID a) can be generically performed if we just always put the ID first
--   this introduces the requirement that ids should always come first.
instance forall a. ToRow a => ToRow (WithUUID a) where
    toRow (WUUID id_ obj) = [toField id_] <> toRow obj

instance forall (state :: TaskState) a. ToRow (Complete (TaskFInState state)) where
    toRow = toRow . toTask

instance forall (state :: TaskState) a. ToRow (Complete TaskF) where
    toRow = toRow . toTaskFromF

instance forall a. FromRow a => FromRow (WithUUID a) where
    -- While normally FromRow instances are written like: `ValueConstructor <$> field <*> field ...`
    -- I can't figure out how to cleanly construct and build the partial result using applicatives
    -- since I need to pull out the ID, set it aside, then work on the rest, *then* use the right GADT constructor for WithId a
    fromRow = field -- pull out first field (we assume it's the ID)
              >>= \idSQLData -> fromRow -- parse the rest of the fields into an `a` value
              >>= chooseCtor idSQLData -- Given the SQLData, use the right GADT constructor on the id + the `a` value
        where
          chooseCtor sqldata = case sqldata of
                                 (SQLText txt) -> \obj -> case fromText txt of
                                                            Just uuid -> pure $ WUUID uuid obj
                                                            Nothing -> throw $ ConversionFailed (show sqldata) "Text" "UUID failed fromText conversion"
                                 _ -> throw $ ConversionFailed (show sqldata) "???" "Unrecognized contents in UUID field (no valid WithID GADT constructor)"

instance FromRow a => FromRow (Identity a) where
    fromRow = Identity <$> (fromRow :: RowParser a)

instance FromField a => FromField (Identity a) where
    fromField = (Identity <$>) . (fromField :: FieldParser a)

instance FromField TaskState where
    fromField f = case fieldData f of
                    SQLText txt -> pure $ read $ DT.unpack txt
                    fd -> returnError ConversionFailed f "Unexpected TaskState field type"

instance forall (state :: TaskState). FromRow (Complete (TaskFInState state)) where
    fromRow = UnknownStateT <$> field <*> field <*> field

disconnectionError :: IO (Either EntityStoreError a)
disconnectionError = pure $ Left $ DisconnectedES "Store is disconnected"

makeGenericInsertError :: SomeException -> IO (Either EntityStoreError a)
makeGenericInsertError = pure . Left . UnexpectedErrorES . ("INSERT command failed: " <>) . DT.pack . show

saveAndReturnTask :: forall (state :: TaskState).
                     Connection
                         -> WithUUID (Complete (TaskFInState state))
                         -> IO (Either EntityStoreError (WithUUID (Complete (TaskFInState state))))
saveAndReturnTask c t = catch doInsert makeGenericInsertError
    where
      doInsert = execute c "INSERT INTO tasks (uuid, name, description, state) VALUES (?,?,?,?)" t
                 >> pure (Right t)

instance FromRow SQLMigrationVersion where
     fromRow = SQLMigrationVersion <$> field

-- | Helper function for making migration failed errors
makeMigrationFailedError :: SQLMigration -> SomeException -> IO (Either MigrationError a)
makeMigrationFailedError m = pure . Left . MigrationQueryFailed from to . DT.pack . show
    where
      from = smFrom m
      to = smTo m

executeMigration :: Connection -> SQLMigration -> IO (Either MigrationError ())
executeMigration conn m = catch runQuery (makeMigrationFailedError m)
    where
      migrationQuery = Query $ getMigrationQuery $ smQuery m
      versionUpdateQuery = Query $ ("PRAGMA user_version = " <>) . DT.pack . show $ getMigrationVersion $ smTo m

      migrateAndUpdateVersion = execute_ conn migrationQuery
                                >> execute_ conn versionUpdateQuery
      runQuery = withTransaction conn migrateAndUpdateVersion
                 >> pure (Right ())

-- | Helper function for making `VersionFetchFailed` `MigrationError`s
makeVersionFetchFailedError :: SomeException -> IO (Either MigrationError a)
makeVersionFetchFailedError = pure . Left . VersionFetchFailed . ("Unexpected version fetch failure: " <>) . DT.pack . show

getDBMigrationVersion :: Connection -> IO (Either MigrationError SQLMigrationVersion)
getDBMigrationVersion c = catch runQuery makeVersionFetchFailedError
    where
      getVersionQuery = Query "PRAGMA user_version;" -- Happens to return 0 if never set before in SQLite
      runQuery = query_ c getVersionQuery
                 >>= \results -> pure $ case results of
                                          (v:_) -> Right v
                                          _     -> Left (VersionFetchFailed "Version retrieval query returned no results")

instance HasMigratableDB SQLiteEntityStore where
    desiredVersion :: SQLiteEntityStore -> IO SQLMigrationVersion
    desiredVersion _ = pure (SQLMigrationVersion 1)

    availableMigrations :: SQLiteEntityStore -> IO [SQLMigration]
    availableMigrations _ = pure $ sort migrations

    getCurrentVersion :: SQLiteEntityStore -> IO (Either MigrationError SQLMigrationVersion)
    getCurrentVersion = maybe _error _handler . stsConn
        where
          _error = pure $ Left $ VersionFetchFailed "Fetching current version failed"
          _handler = getDBMigrationVersion

    migrateTo :: SQLiteEntityStore -> ToSQLMigrationVersion -> IO (Either MigrationError ())
    migrateTo s expected = maybe unexpectedMigrationErr tryHandler $ stsConn s
        where
          unexpectedMigrationErr :: IO (Either MigrationError ())
          unexpectedMigrationErr = pure $ Left $ UnexpectedMigrationError "Failed to retrieve DB connection"

          convertToUnexpectedError :: SomeException -> IO (Either MigrationError ())
          convertToUnexpectedError = pure . Left . UnexpectedMigrationError . DT.pack . show

          tryHandler :: Connection -> IO (Either MigrationError ())
          tryHandler conn = catch (handler conn) convertToUnexpectedError

          -- | Recursively (!) runs all migrations by
          --   There's quite a bit of wasted effort in here, but it's probably good enough (assuming it finishes :)).
          handler :: Connection -> IO (Either MigrationError ())
          handler conn = availableMigrations s
                         -- ^ Get the list of current migrations
                         >>= \usableMigrations -> getCurrentVersion s
                         -- ^  Get the current version
                         >>= rightOrThrow
                         -- ^  Get the current version
                         >>= \current -> pure (findNextMigration usableMigrations current)
                         -- ^ Determine the next migration
                         >>= \case
                             -- | We're either done or something went wrong
                             Nothing -> pure $ if current == expected then Right () else Left NoMigrationPath
                             -- | Perform a single migration then recur
                             Just m -> executeMigration conn m
                                       >>= rightOrThrow
                                       >> handler conn

          -- | We are assuming monotonically increasing version numbers here, and that there exists at least
          --   *one* migration between every version (i.e. v1->v2, v2->v3, etc). This is a bad assumption to make generally,
          --   but I'm OK with it since this is generally how most people make migrations in my mind, implementation can change later if need be
          isNextStep current migration = smFrom migration == current && smTo migration == current + 1
          findNextMigration migrations current = find (isNextStep current) migrations

instance Constructable SQLiteEntityStore CompleteEntityStoreConfig EntityStoreError where
    construct :: CompleteEntityStoreConfig -> IO (Either EntityStoreError SQLiteEntityStore)
    construct cfg = catch makeStore connectionFailure
        where
          dbPath :: String
          dbPath = runIdentity $ escDBFilePath cfg

          makeStore :: IO (Either EntityStoreError SQLiteEntityStore)
          makeStore = open dbPath
                      >>= \conn -> pure (Right (SQLiteEntityStore cfg (Just conn)))

          connectionFailure :: SomeException -> IO (Either EntityStoreError SQLiteEntityStore)
          connectionFailure = pure . Left . ConnectionFailureES . ("Failed to connect to DB: "<>) . DT.pack . show

withActiveConn :: SQLiteEntityStore -> (Connection -> IO (Either EntityStoreError a)) -> IO (Either EntityStoreError a)
withActiveConn store action = maybe disconnectionError action $ stsConn store

-- | Ensure that a UUID is present on a given entity
ensureUUID :: entity -> IO (Either EntityStoreError (WithUUID entity))
ensureUUID e = Right . flip WUUID e <$> nextRandom

-- | Insert and return an entity
insertAndReturnEntity :: forall entity.
                         ( SQLInsertable (WithUUID entity)
                         , SQLInsertable entity
                         , FromRow (WithUUID entity)
                         )
                        => Connection
                            -> WithUUID entity
                            -> IO (Either EntityStoreError (WithUUID entity))
insertAndReturnEntity conn entity@(WUUID uuid _) = insertEntity conn entity
                                                   >> getEntityByUUID conn uuid
instance SQLInsertable Task where
    tableName    = TN "tasks"
    columnNames  = SQLCN ["name", "description", "state"]

instance forall (state :: TaskState). SQLInsertable (Complete (TaskFInState state)) where
    tableName    = let (TN tbl) = (tableName :: TableName Task) in TN tbl
    columnNames  = let (SQLCN cols) = (columnNames :: SQLColumnNames Task) in SQLCN cols

instance SQLInsertable (Complete TaskF) where
    tableName    = let (TN tbl) = (tableName :: TableName Task) in TN tbl
    columnNames  = let (SQLCN cols) = (columnNames :: SQLColumnNames Task) in SQLCN cols

instance ToRow Task where
    toRow (Task n d s) = toRow (n, d, s)

instance FromRow Task where
    fromRow = Task <$> field <*> field <*> field

instance FromRow (Complete TaskF) where
    fromRow = TaskF <$> field <*> field <*> field

-- | If some value e is SQLInsertable, then the same value with a UUID is insertable
--   All we do is ensure the columns include a "uuid" column at the beginning
instance forall e. SQLInsertable e => SQLInsertable (WithUUID e) where
    tableName = TN tbl
        where
          (TN tbl) = tableName :: TableName e

    columnNames = SQLCN $ "uuid":innerCols
        where
          (SQLCN innerCols) = columnNames :: SQLColumnNames e

instance SQLUpdatable (Partial TaskF) where
    updateColumnGetters _ = [ ("name",        (toField <$>) . tfName)
                            , ("description", (toField <$>) . tfDesc)
                            , ("state",       (toField <$>) . tfState)
                            ]

instance SQLUpdatable (Partial (TaskFInState state)) where
    updateColumnGetters NotStartedT{} = [ ("name",        \(NotStartedT name _) -> toField <$> name)
                                        , ("description", \(NotStartedT _ desc) -> toField <$> desc)
                                        ]

    updateColumnGetters InProgressT{} = [ ("name",        \(InProgressT name _) -> toField <$> name)
                                        , ("description", \(InProgressT _ desc) -> toField <$> desc)
                                        ]

    updateColumnGetters FinishedT{} = [ ("name",        \(FinishedT name _) -> toField <$> name)
                                      , ("description", \(FinishedT _ desc) -> toField <$> desc)
                                      ]

    updateColumnGetters UnknownStateT{} = [ ("name",        \(UnknownStateT name _    _ ) -> toField <$> name)
                                          , ("description", \(UnknownStateT _    desc _ ) -> toField <$> desc)
                                          , ("state",       \(UnknownStateT _    _    state) -> toField <$> state)
                                          ]

data QueryWithParams p = QWP Query p

instance SQLDeletable Task where
    deletionMode = Hard

instance forall (state :: TaskState). SQLDeletable (TaskFInState state) where
    deletionMode = case (deletionMode :: DeletionMode Task) of
                     Hard -> Hard
                     Soft -> Soft

instance SQLDeletable a => SQLDeletable (Complete a) where
    deletionMode = case (deletionMode :: DeletionMode a) of
                     Hard -> Hard
                     Soft -> Soft

instance SQLDeletable a => SQLDeletable (WithUUID a) where
    deletionMode = case (deletionMode :: DeletionMode a) of
                     Hard -> Hard
                     Soft -> Soft

-- | Build the insertion SQL query for a given entity with it's ID
buildInsertQuery :: forall entity. SQLInsertable entity => entity -> QueryWithParams entity
buildInsertQuery = QWP insertQuery
    where
      (TN tbl) = tableName :: TableName entity
      (SQLCN cols) = columnNames  :: SQLColumnNames entity

      columnPhrase = DT.intercalate "," cols
      valueQs = DT.intercalate "," $ replicate (length cols) "?"
      insertQuery = Query $ [text| INSERT INTO $tbl ( $columnPhrase ) VALUES ( $valueQs ) |]

-- | Do the actual insertion for an entity
insertEntity :: forall entity. SQLInsertable entity => Connection -> entity -> IO (Either EntityStoreError ())
insertEntity conn e = Right <$> execute conn query params
    where
      (QWP query params) = buildInsertQuery e

-- | Retrieve an entity by UUID
getEntityByUUID :: forall entity.
                   ( SQLInsertable entity
                   , FromRow entity
                   , ToRow entity
                   )
                   => Connection
                       -> UUID
                       -> IO (Either EntityStoreError entity)
getEntityByUUID conn uuid = query conn selectQuery (Only uuid)
                            -- ^ TODO: Dangerous IO (add try/catch)
                            >>= \case
                                    (v:_) -> pure $ Right v
                                    _     -> pure $ Left $ NoSuchEntityES uuid $ "Failed to find task with UUID [" <> toText uuid <> "]"

    where
      (TN tbl) = tableName :: TableName entity
      selectQuery = Query $ [text| SELECT * FROM $tbl WHERE uuid = ? |]

updateEntityByUUID :: forall entity.
                   ( SQLInsertable (Complete entity)
                   , SQLUpdatable (Partial entity)
                   , FromRow (Complete entity)
                   , FromRow (WithUUID (Complete entity)))
                   => Connection
                       -> UUID
                       -> Partial entity
                       -> IO (Either EntityStoreError (WithUUID (Complete entity)))
updateEntityByUUID conn uuid partial = case cols of
                                         [] -> badPartialErr
                                         _ -> withTransaction conn updateAndCheckChanges
                                              >>= \case
                                                  1 -> getEntityByUUID conn uuid
                                                  _ -> noRowsChangedErr
    where
      (TN tbl) = tableName :: TableName (Complete entity)
      (SQLCN cols) = updateColumns partial

      badPartialErr = pure $ Left $ UnexpectedErrorES "Update contains no values"
      noRowsChangedErr = pure $ Left $ UnexpectedErrorES "Update happened but no rows were changed"

      values = updateValues partial
      valuesWithID = values <> [toField uuid]
      setPhrase = DT.intercalate "," $ (<>"=?") <$> cols

      updateQuery = Query $ [text| UPDATE $tbl SET $setPhrase WHERE uuid=? |]

      updateAndCheckChanges = execute conn updateQuery valuesWithID
                              >> changes conn

deleteEntityByUUID :: forall entity.
                   ( SQLInsertable entity
                   , SQLDeletable entity
                   , FromRow entity
                   )
                   => Connection
                       -> UUID
                       -> IO (Either EntityStoreError entity)
deleteEntityByUUID conn uuid = getEntityByUUID conn uuid
                               >>= rightOrThrow
                               >>= \beforeDelete -> withTransaction conn deleteAndCheckChanges
                               >>= \case
                                   1 -> pure $ Right $ beforeDelete
                                   _ -> pure $ Left $ UnexpectedErrorES "Delete failed, no rows were changed"
    where
      (TN tbl) = tableName :: TableName entity

      deleteQuery = case deletionMode :: DeletionMode entity of
                      Hard -> Query $ [text| DELETE FROM $tbl WHERE uuid = ? |]
                      Soft -> Query $ [text| UPDATE $tbl SET deleted=1 WHERE uuid = ? |]

      deleteAndCheckChanges = execute conn deleteQuery (Only uuid)
                              >> changes conn

-- | List entities
listEntities :: forall entity.
                ( SQLInsertable entity
                , SQLInsertable entity
                , FromRow entity)
               => Connection
                   -> IO (Either EntityStoreError [entity])
listEntities conn = Right <$> query_ conn selectAllQuery
    where
      (TN tbl) = tableName :: TableName entity
      selectAllQuery = Query $ [text| SELECT * FROM $tbl |]

-- | Generalized typeclass for entity storage.
instance SQLEntityStore SQLiteEntityStore where
    create store (Validated entity)  = withActiveConn store _work
        where
          _work conn = ensureUUID entity
                    >>= rightOrThrow
                    -- | Generate an insert query for the `WithID entity`
                    >>= insertAndReturnEntity conn

    getByUUID store uuid = withActiveConn store _work
        where
          _work conn = getEntityByUUID conn uuid

    updateByUUID store uuid (Validated partial) = withActiveConn store _work
        where
          _work conn = updateEntityByUUID conn uuid partial

    deleteByUUID store uuid = withActiveConn store _work
        where
          _work conn = deleteEntityByUUID conn uuid

    list store = withActiveConn store _work
        where
          _work conn = listEntities conn
