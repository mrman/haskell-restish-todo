{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}

module Components.EntityStore.SQLiteSpec (spec) where

import Components.EntityStore.SQLite (SQLiteEntityStore)
import Control.Monad (when)
import Types
import Data.Either (isLeft)
import Data.Functor.Identity
import Config (defaultCompleteEntityStoreConfig)
import Data.Either (isRight)
import Control.Monad.IO.Class (liftIO)
import Util (rightOrThrow)
import Data.UUID (toText)

import Test.Hspec

makeDefaultStore :: IO (Either EntityStoreError SQLiteEntityStore)
makeDefaultStore = construct defaultCompleteEntityStoreConfig

-- generateTask :: IO (Validated NotStartedTask)
generateTask :: IO (Validated (Complete NotStartedTask))
generateTask = rightOrThrow $ validate $ NotStartedT name desc
    where
      name = Identity "example"
      desc = Identity "this is a example task"

generateTaskNameUpdate :: IO (Validated (Partial NotStartedTask))
generateTaskNameUpdate = rightOrThrow $ validate $ NotStartedT (Just "updated name") Nothing

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "entity store creation" $
         it "works with the default config" $ \_ -> liftIO makeDefaultStore
                                                    >>= (`shouldBe` True) . isRight

  describe "entity store migration" $
         it "migrates with the default config (0 -> 1)" $ \_ -> liftIO makeDefaultStore
                                                                >>= rightOrThrow
                                                                >>= liftIO . migrate
                                                                >>= shouldBe (Right ())

  describe "entity store create" $
         it "works with default config" $ \_ -> liftIO makeDefaultStore
                                                >>= rightOrThrow
                                                >>= \store -> migrate store
                                                >> generateTask
                                                >>= \expected -> (create store expected :: IO (Either EntityStoreError (WithUUID (Complete NotStartedTask))))
                                                >>= rightOrThrow
                                               -- | Ensure that the ID is non-empty when printed, and the object we got back is right
                                                >>= \(WUUID uuid task) -> pure (toText uuid /= "" && task == getValidatedObj expected)
                                                >>= (`shouldBe` True)

  describe "entity store update" $
         it "works with default config" $ \_ -> liftIO makeDefaultStore
                                                >>= rightOrThrow
                                                >>= \store -> migrate store
                                                >> generateTask
                                                >>= \original -> (create store original :: IO (Either EntityStoreError (WithUUID (Complete NotStartedTask))))
                                                >>= rightOrThrow
                                                -- | Generate and perform update
                                                >>= \expected@(WUUID uuid _) -> generateTaskNameUpdate
                                                >>= updateByUUID store uuid
                                                >>= rightOrThrow
                                               -- | The task should have changed
                                                >>= \returned@(WUUID uuid task) -> pure (toText uuid /= "" && returned /= expected)
                                                >>= (`shouldBe` True)

  describe "entity store delete" $
           it "works with default config" $ \_ -> liftIO makeDefaultStore
                                                >>= rightOrThrow
                                                >>= \store -> migrate store
                                                >> generateTask
                                                -- | Create a task
                                                >>= \original -> create store original
                                                >>= rightOrThrow
                                                -- | Delete the created task right after creating it
                                                >>= \(WUUID uuid _) -> (deleteByUUID store uuid :: IO (Either EntityStoreError (WithUUID (Complete NotStartedTask))))
                                                >>= rightOrThrow
                                                -- | Ensure task returned by the deletion matches created one
                                                >>= \(WUUID _ obj) -> when (obj /= getValidatedObj original) (error "returned deleted object mismatch")
                                                -- | Ensure that a get with the deleted item's ID fails (produces a Left value)
                                                >> (getByUUID store uuid :: IO (Either EntityStoreError (WithUUID (Complete NotStartedTask))))
                                                >>= (`shouldSatisfy` isLeft)

  describe "entity store list" $
         it "works with default config" $ \_ -> liftIO makeDefaultStore
                                                >>= rightOrThrow
                                                >>= \store -> migrate store
                                                >> generateTask
                                                >>= \expected -> create store expected
                                                >>= rightOrThrow
                                                -- | Ensure that the ID is non-empty when printed, and the object we got back is right
                                                >> (list store :: IO (Either EntityStoreError [WithUUID Task]))
                                                >>= rightOrThrow
                                                >>= (`shouldBe` 1) . length
