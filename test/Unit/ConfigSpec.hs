module ConfigSpec (spec) where

import Test.Hspec
import Config as C
import Data.Functor.Identity

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "defaults" $ do
         it "has localhost as the default host" $
            C.defaultHost `shouldBe` "localhost"

         it "has 5000 as the default port" $
            C.defaultPort `shouldBe` 5000

  describe "default values" $ do
         it "CompleteAppConfig has default host" $
           host C.defaultCompleteAppConfig `shouldBe` Identity C.defaultHost
         it "CompleteAppConfig has default port" $
            port C.defaultCompleteAppConfig `shouldBe` Identity C.defaultPort

         it "PartialAppConfig has no default host" $
           host C.defaultPartialAppConfig `shouldBe` Nothing
         it "PartialAppconfig has no default port" $
            port C.defaultPartialAppConfig `shouldBe` Nothing
